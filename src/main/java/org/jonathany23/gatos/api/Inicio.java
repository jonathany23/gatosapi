package org.jonathany23.gatos.api;

import javax.swing.*;

public class Inicio {
    public static void main(String[] args) {
        int optionMenu = -1;
        String[] botones = {"1. Ver Gatos", "2. Salir"};

        do {
            String opcion = (String) JOptionPane.showInputDialog(null, "Gatitos Java",
                    "Menu Principal", JOptionPane.INFORMATION_MESSAGE, null, botones, botones[0]);

            //Validamos que opcion selecciona el usuario
            for (int i = 0; i < botones.length; i++) {
                if (opcion.equals(botones[i])){
                    optionMenu = i;
                }
            }

            switch (optionMenu){
                case 0:
                    try {
                        GatosService.verGatos();
                    } catch (Exception e){
                        System.err.println(e);
                    }
                    break;
                default:
                    break;
            }
        } while (optionMenu != 1);
    }
}
