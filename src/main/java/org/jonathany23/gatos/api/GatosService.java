package org.jonathany23.gatos.api;

import com.google.gson.Gson;
import com.squareup.okhttp.*;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.net.URL;

public class GatosService {

    public static void verGatos() throws IOException {
        //1. Vamos a traer los datos de la API
        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url("https://api.thecatapi.com/v1/images/search")
                .get()
                .build();

        Response response = client.newCall(request).execute();

        String json = response.body().string();

        //Cortar corchetes iniciales
        //Descarta Primer caracter
        json = json.substring(1, json.length());
        //Descarta ultimo caracter
        json = json.substring(0, json.length()-1);

        //Crear objeto de la clase Gson
        Gson gson = new Gson();
        Gatos gatos = gson.fromJson(json, Gatos.class);

        //Redimencionar en caso de necesitar
        Image image = null;
        try{
            URL url = new URL(gatos.getUrl());
            image = ImageIO.read(url);
            ImageIcon fondoGato = new ImageIcon(image);

            if (fondoGato.getIconWidth() > 600){
                //redimencionar
                Image fondo = fondoGato.getImage();
                Image imgModificada = fondo.getScaledInstance(800, 600, Image.SCALE_SMOOTH);
                fondoGato = new ImageIcon(imgModificada);
            }

            String menu = "Opciones: \n" +
                    "1. Ver Otra imagen \n" +
                    "2. Favorito \n" +
                    "3. Volver \n";

            String [] botones = {"ver otra imagen", "favorito", "volver"};
            String idGato = gatos.getId();
            String opcion = (String) JOptionPane.showInputDialog(null, menu, idGato,
                    JOptionPane.INFORMATION_MESSAGE, fondoGato, botones,botones[0]);

            int seleccion = -1;

            //Validamos que opcion selecciona el usuario
            for (int i = 0; i < botones.length; i++) {
                if (opcion.equals(botones[i])){
                    seleccion = i;
                }
            }

            switch (seleccion){
                case 0:
                    verGatos();
                    break;
                case 1:
                    favoritoGato(gatos);
                    break;
                default:
                    break;
            }

        } catch (IOException e){
            System.err.println(e);
            e.printStackTrace();
        }
    }

    public static void favoritoGato(Gatos gatos){
        try {
            OkHttpClient client = new OkHttpClient();

            MediaType mediaType = MediaType.parse("application/json");
            RequestBody body = RequestBody.create(mediaType, "{\n  \"image_id\": \""+gatos.getId()+"\"\n}");
            Request request = new Request.Builder()
                    .url("https://api.thecatapi.com/v1/favourites")
                    .post(body)
                    .addHeader("Content-Type", "application/json")
                    .addHeader("x-api-key", gatos.getApiKey())
                    .build();

            Response response = client.newCall(request).execute();
        } catch (Exception e){
            System.err.println(e);
        }
    }
}
